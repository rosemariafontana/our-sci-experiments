stages:
  - configure
  - retrieve
  - test
  - deploy

image: node:latest

cache:
  paths:
    - node_modules/
    - scripts/docusaurus/farm_schemas/node_modules
    - input/collection/
    - input/farmos.json
    - output/validators/
    - output/collection/
    - output/documentation/
    - public/

# Install node libraries, this stage is cached to avoid excesive usage of CI minutes
install_dependencies:
  stage: configure
  when: always
  script:
    - echo 'Installing Node dependencies'.
    - npm ci
    - mkdir -p input/collection
    - mkdir -p output/validators
    - mkdir -p output/collection/conventions
    - mkdir -p output/collection/overlays
    - mkdir -p output/documentation


copy_schemas:
  stage: retrieve
  when: always
  allow_failure: true
  script:
    - echo 'Retrieving FarmOS schemata from model farm'.
    - node ./scripts/getAllSchemas.js
    - node ./scripts/compileAllValidators.js
    - bash ./scripts/rebuildCollection.sh
  artifacts:
    paths:
      - input/collection/
      - output/validators/
      - output/documentation/
      - output/collection/
      - input/farmos.json
    when:
      always

test_example_conventions:
  stage: test
  when: always
  script:
    - npm test specific_conventions
  artifacts:
    when: always
    paths:
      - test/test_results/
    expire_in: 30 days
    reports:
      junit: test/test_results/*.xml

test_conventions:
  stage: test
  when: always
  script:
    - npm test conventions_against_examples
  artifacts:
    when: always
    paths:
      - test/test_results/
    expire_in: 30 days
    reports:
      junit: test/test_results/*.xml

# Transpiles and publishes the validator package to npm.
publish_package:
  stage: deploy
  interruptible: false
  only:
    - main
  script:
    - node ./scripts/transpilation.js
    - bash ./scripts/publishPackage.sh

pages:
  stage: deploy
  script:
    - mkdir -p scripts/docusaurus/docs/Conventions
    - ./scripts/publishWiki.sh "$CI_COMMIT_BRANCH"
  artifacts:
    paths:
      - public
  only:
    - main
    - staging
