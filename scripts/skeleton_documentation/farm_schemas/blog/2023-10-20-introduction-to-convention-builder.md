---
slug: using-the-convention-builder
title: How to define your own conventions
authors:
  name: Octavio M. Duarte
  title: OurSci, LLC.
  url: https://gitlab.com/OctavioDuarte
  image_url: https://gitlab.com/uploads/-/system/user/avatar/5581361/avatar.png
tags: [json_schema, conventions, code, node, javascript]
---

# Introduction

  While we help design a _standard_ for conventions, we are also developing tools to make writing them as easy as possible.
  
  Designing your convention using this tools has many advantages. Not only is it easier and faster:
  
  * By one side, the function's high level interface will help you stick  to the standard and validate your work at all times. 
  * By the other, if the standard evolves over time, most of the times the interface will remain untouched and your conventions will updated by interpreting your commands (your intention) using the new rules/features. 
  
  This makes your effort more effective and durable over time, whereas if you were to write your conventions as statics **JSON schema** files, each time an update is added to the standard you would need to painstakingly review one by one and change as needed. 
  
  The **NodeJS** library we created is called *convention_builder.js*. In this post we will explain its proposed usage and its current features.
  
  
# Main Design Intentions for the Library


  The library will not only help you create your conventions, but also communicate them between each other, keep them coherent, share and document them and constantly validate your work via different strategies. All of these features are obtained with minimum effort, almost the only user intervention needed is designing the convention.
  
  The process of building a convention always starts with picking several [ _FarmOS_ ](https://farmos.org/model/) entities that you need to encode your data, as well as a proper way to link them. These entities are the main building blocks in this system. Our work will be to refine them by adding  more restrictions and choosing a set of them to relate to each other in a specific way.
  
  Each entity type has several bundles, representing refined options. We will look into their `attributes` and allowed `relationship` to decide on which one is the more suitable. They are fairly atomic, so we will typically need several of them to achieve a reasonable level of detail.
  
  After that selection is done, you will be able to encode your requirements over each one of the individual entities by creating _overlays_, and then you will be able to create a global and linked structure by tying all of your _overlays_ into a fully fledged _convention_.
  
  
# First Example


  As a first example, we will design a **convention** for a planting. It will represent the vegetables on a single parcel. We will attach it to any piece of information we know will always be needed when mentioning it.
  In this case, let's assume that information is a **species**, a **variety**, a **location** and a **season**.
  
  In order to invest further sense into the examples, let's say we are the administrators of a tiny orchard, which only works with some species (_tomato_, _lettuce_ and _corn_ ).
  
  
## Selecting the Entity Types we Need

  
  You can get acquanted with the general organization of entities by looking into the link we provided before, [upstream from their creators](https://farmos.org/model/).
  
  Besides, if you are working with a clone of our repo, the script `getAllSchemas.js` will provide you with a folder representing your **input collection**, under `/input/collection`. Inside, entities are stored hierarchically and you can look into the schemata. The key elements are its _attributes_ (properties defining the entity itself) and _relationships_ (properties tied to the entity by linking it to other entities).
  
  In our case, after carefully looking, we decide the main entit we need to represent a _planting_ in this project is an `asset--plant`.
  
  Species, variety and season are all elements that we typically pick from a list (a list of species, a list of varieties for that species, a list of seasons in which a field has been active, etc). For these, FarmOs offers *taxonomy_term* entities. Both **species** and **variety** will be `taxonomy_term--plant_type`, and **seasons** have their own type, `taxonomy_term--season`.
  

### Selected Types:

1. The planting itself: `asset--plant`.
1. Species: `taxonomy_term--species`.
1. Variety: `taxonomy_term--species`.
1. Season: `taxonomy_term--season`.
  
  
## Overlays: Designing each entity individually


### Generalities


  Overlays are sets of retrictions and precissions imposed over an entity of a certain type. An **Object Class** represents them in our library, providing us with methods that will help us refine it. The class is documented [here](https://our-sci.gitlab.io/software/json_schema_distribution/SchemaOverlay.html).
  
  There are three main tools that will help us refine it. These are `.setConstant`, `.setEnum` and `.setPattern`. Each one will need an `attribute` as its first argument. `attributes` are the entity's properties, as defined in its schema (we will see examples in the next section). They are typically very open ended, having only a rough type such as `string` or `double`. While writing a convention, we typically know way more about them, and our schema will ensure the values presented to it are realistic if we represent these tight restrictions. For example: we know our only crops are tomato, lettuce and corn, and our overlay will reflect that.
  
  Besides this, we can make some arguments **mandatory** by marking them as `required` and we will also **need** to set descriptions, wich will feed the automatically generated documentation.
  
### First overlay: our Plant Asset


  Let's start writing our script. I recommend working from a node REPL or evaluating the script frequently, as most functions and classes have a very reponsive interface and should guide you towards your aim. We will review how at each step. Let's start by calling our library, building the overlay and seeint how it looks like.
  

#### Creating the object

  
```js
// import our library
const builder = require(`../src/convention_builder`);

// define our first overlay, the planting
let plantingPlantAsset = new builder.SchemaOverlay({
    // this is the type and bundle, which should be available in our input collection
    typeAndBundle: 'asset--plant',
    // this is a name used internally. The overlay will always be named by adding this together with the typeandBundle, so this one will be called 'asset--plant--planting'
    name: 'planting',
    // we have the option to provide valid and invalid examples, which allows to control if we are effectively accepting the cases we want to accept and rejecting the ones we want to reject.
    // validExamples:,
    // erroredExamples:    
});
```
  

#### Looking inside the unfinished object


  Even though we will need to add some stuff yet (you can even see I commented some of it out), I recommend evaluating early and interacting with the object. 
  A first advantage is the **schema is provided inside the object**, which is comfortable to observe its contents and help us think about our design.


```js
// let's see the schema
console.log(plantingPlantAsset.schema)


{
  '$id': 'https://ourscitest.farmos.net/api/asset/plant/resource/schema?farm_id=1',
  title: 'Plant asset',
  type: 'object',
  properties: {
    id: { type: 'string', format: 'uuid' },
    type: { const: 'asset--plant' },
    meta: { type: 'object' },
    attributes: {
      description: 'Entity attributes',
      type: 'object',
      properties: [Object],
      required: [Array],
      additionalProperties: false
    },
    relationships: {
      description: 'Entity relationships',
      properties: [Object],
      type: 'object',
      required: [Array],
      additionalProperties: false
    }
  },
  description: undefined
}
```


Let's zoom in into the `attributes` first. I abbreviated some of them in order to help us better see what we care the most about.



```js
// let's see the schema
console.log(plantingPlantAsset.schema.properties.attributes.properties)

{
  .
  .
  .
  name: {
    type: 'string',
    title: 'Name',
    maxLength: 255,
    description: 'The name of the asset.'
  },
  status: {
    type: 'string',
    title: 'Status',
    maxLength: 255,
    description: 'Indicates the status of the asset.',
    default: 'active'
  },
  geometry: {
    type: 'object',
    properties: {
      value: [Object],
      geo_type: [Object],
      lat: [Object],
      lon: [Object],
      left: [Object],
      top: [Object],
      right: [Object],
      bottom: [Object],
      geohash: [Object],
      latlon: [Object]
    },
    title: 'Current geometry'
  },
  .
  .
  .
}


// let's also have a look at the 'required' attributes, the ones we are forced to fill in order to get a valid entity
console.log(plantingPlantAsset.schema.properties.attributes.required)


[ 'name', 'status' ]
```

* `name` and `status` are required, and we need to fill them for every valid entity.
* `geometry` is used to add cartographic properties. In this case, we have other conventions in mind, and we decided to not represent geometry into the plantings, but instead into `field` conventions we will define later. Therefore, we will null this one.
* We will leave all other properties untouched (as free as in the original definition). 
* We will **document** everything thoroughly.

#### Describing our entitiy so other people can understand what it represents

```js
plantingPlantAsset.setMainDescription("This overlay represents plants as they are encoded in Octavio's farm, a medium sized comercial orchard in Argentina.");
```

#### Applying our restrictions

1. For **status**, we know in our farm information is only registered after the plant is productive, so we will fix it to a unique value, `active`. This can be achieved by using the `setConstant` feature. `geometry` will also be a constant, `null`.
2. For **name** we want to ensure it follorws our farm's format. We will encode the format as regex and use the `setPattern` feature.

```js
plantingPlantAsset.setConstant({
    // mention what we are fixing to a constant
    attribute:"status",
    // choose the fixed value
    value:"active",
    // explain why we do this
    description:""
});

plantingPlantAsset.setConstant({
    attribute:"geometry",
    value:null,
    description: "Geometry will be stored into the 'field' convention, therefore we omit it here."
});
```


  All of our planting names follow a strict convention: the involved species name, a 4 character alphanumeric code and the current decade, represented by two further numbers. A regex pattern that enforces this is `((lettuce)|(tomato)|(corn))-([a-z0-9]{4})-([0-9]{2})`.

```js
plantingPlantAsset.setPattern({
    // as with the other restriction we've seen (setConstant), we choose a target attribute between the ones defined by the schema.
    attribute:"name",
    // Instead of a unique value, we provide a regular expression representing how we encode our planting names.
    pattern:"((lettuce)|(tomato)|(corn))-([a-z0-9]{4})-([0-9]{2})",
    description: "This regular expression represents the planting name format in this farm: the involved species name, a 4 character alphanumeric code and the current decade, represented by two further numbers."
});
```


  This basically summarises what we want to control in our `asset--plant` entities. Are we done? We could, but we have the option to provide valid and invalid examples into our object. Our schema will be constantly tested against said examples, so it is a great advantage to have them early and make our process as robust as possible. 
  

#### Adding our examples


  We can both provide examples we expect to **fail** in the `erroredExamples` array, and examples we intend to **succeed** in `validExamples`.
  
  Once they are in position, we can see if the schema accepts them or not and, in the case of rejected examples, *why*.
  
  Since we need to provide an `id` attribute to get a basic entity, I will import a function, `crypto.randomUUID`. As we will later work on `relationships` across different entities, I will store the `id` in a variable and use the same id for all examples (so that they are interchangeable when testing over a larger structure).
  
  I will create several examples and comment above them why each one is relevant.
  
  
```js

// adding the new function we need
const { randomUUID } = require('crypto');


let plantAssetUUID = randomUUID();

// first, I want to ensure valid examples are accepted for each crop I'm cultivating.
let validTomato = {
    id:plantAssetUUID,
    attributes: {
        name: "tomato-ae7y-23",
        geometry: null,
        status:"active"
    }
};
let validLettuce = {
    id:plantAssetUUID,
    attributes: {
        name: "lettuce-4e7y-22",
        geometry: null,
        status:"active"
    }
};
let validCorn = {
    id:plantAssetUUID,
    attributes: {
        name: "corn-4e8y-23",
        geometry: null,
        status:"active"
    }
};

let validPlantAssetExamples = [
    validTomato, validLettuce, validCorn
];

// I will create examples with invalid names to ensure my pattern is working and also with a missing status.
let errorInDecadeNoStatus = {
    id:plantAssetUUID,
    attributes: {
        // this decade has only one instead of the two mandatory digits
        name: "tomato-ae7y-3",
        geometry: null,
        // the enforced property 'status' is missing
    }
};
let unknownCropWrongStatus = {
    id:plantAssetUUID,
    attributes: {
        // this is not contemplated by our pattern
        name: "chard-ae7y-23",
        geometry: null,
        // the enforced property 'status' has a value other than our constant
        status: "ready"
    }
};

let plantAssetErrorExamples = [ errorInDecadeNoStatus, unknownCropWrongStatus ];

// let's populate the fields that were empty in our overlay with these new examples.
plantingPlantAsset.validExamples = validPlantAssetExamples;
plantingPlantAsset.erroredExamples = plantAssetErrorExamples;

// let's evaluate if our examples turn out as expected
let plantAssetTestResult = plantingPlantAsset.testExamples();
```

  Lets have a look at the results of our test.
  

#### Analizing the testExamples object.


  The `testExamples` object can be a little confusing due to the fact that it answers questions such as 'Is the false example true?'. 
  
  The basic idea is, irrespective of the conditions, `success: true` means we got what we wanted.
  
  We always attach the analyzed entity so that it is easier to check what we are speaking about, and in case there are errores, there's an `errors` object, showing the objections generated by the **schema**.
  
  
```js
console.log(plantAssetTestResult);

{
  // a valid example is 'success: true' when it is accepted by our schema.
  validExamples: [
    { valid: true, success: true, entity: [Object] },
    { valid: true, success: true, entity: [Object] },
    { valid: true, success: true, entity: [Object] }
  ],
  // an errored example us 'success: true' when it is rejected by our schema as intended.
  erroredExamples: [
    { valid: false, success: true, errors: [Array], entity: [Object] },
    { valid: false, success: true, errors: [Array], entity: [Object] }
  ],
  success: true,
  failedExamples: []
}
```


  Since we added many errors into each errored example, it is worth checking if all of them were detected.
  
```js
// errors detected in our first error example
console.log(plantAssetTestResult.erroredExamples[0].errors);


[
  {
    instancePath: '/attributes/name',
    schemaPath: '#/properties/attributes/properties/name/pattern',
    keyword: 'pattern',
    params: { pattern: '(lettuce)|(tomato)|(corn)-[a-z0-9]{4}-[0-9]{2}' },
    // it failed to match the patter, as the "decade" section has only one digit, we used "tomato-ae7y-3".
    message: 'must match pattern "(lettuce)|(tomato)|(corn)-[a-z0-9]{4}-[0-9]{2}"'
  },
  {
    instancePath: '/attributes',
    schemaPath: '#/properties/attributes/required',
    keyword: 'required',
    params: { missingProperty: 'status' },
    // it detected that the required property is absent.
    message: "must have required property 'status'"
  }
]

// errors detected in our second error example
console.log(plantAssetTestResult.erroredExamples[1].errors)
[
  {
    instancePath: '/attributes/name',
    schemaPath: '#/properties/attributes/properties/name/pattern',
    keyword: 'pattern',
    params: { pattern: '(lettuce)|(tomato)|(corn)-[a-z0-9]{4}-[0-9]{2}' },
    // this time we used a non allowed species for the patter, (chard). It also triggered an error.
    message: 'must match pattern "(lettuce)|(tomato)|(corn)-[a-z0-9]{4}-[0-9]{2}"'
  },
  {
    instancePath: '/attributes/status',
    schemaPath: '#/properties/attributes/properties/status/const',
    keyword: 'const',
    params: { allowedValue: 'active' },
    // this time, the value we provided (ready) is different to the constant, and is rejected.
    message: 'must be equal to constant'
  }
]
```


###  The Species Overlay

  Before working on it, let's look at the schema.
  
#### base schema

  Let's directly look into the `attributes` section.
  
```js
{
  properties: {
  .
  .
  .
    name: { type: 'string', title: 'Name', maxLength: 255 },
    .
    .
    .
  },
  required: [ 'name' ],
  additionalProperties: false
}
```


#### Steps we already know


  As this is a taxonomy term, a means to register names of species, we only care about the name, which incidentally is the only **required** attribute.
  Now that we've seen the dynamic of working with overlays in detail, I'll go faster except when there's a remarcable difference.
  
```js
// let's first create the examples, and the UUID we need to create them.
let speciesUUID = randomUUID();
let speciesExample = {
    id: speciesUUID,
    attributes: {
        name: "corn"
    }
};
let speciesError = {
    id: speciesUUID,
    attributes: {
        // let's choose a species we are not cultivating
        name: "kale"
    }
};

// now, let's create the full overlat at once
let plantingSpecies = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--plant_type",
    name: "species",
    validExamples: [speciesExample],
    erroredExamples: [speciesError],
});
// remember to document your work!
plantingSpecies.setMainDescription("The species of the vegetable planted. This establishment only works with tomato, lettuce, and corn.");
```

#### A new method

  In this case, there's one detail we might care about: since we know which **species** we work with, we would like to enforce that on the overlay. Luckily, that's possible using our latest restriction method, `.setEnum`, which allows us to feed in array of acceptable values, and will reject anything else.

```js
// let's limit the species name to the ones we know we will deal with.
plantingSpecies.setEnum({
    attribute:"name",
    valuesArray:["tomato", "lettuce", "corn"],
    description:"Limited to a subset of know crops.",
});
```


#### Testing our examples

  
  Testing the examples shows we've achieved what we wanted.
 

```js
console.log(plantingSpeciesTest)
{
  validexamples: [ { valid: true, success: true, entity: [object] } ],
  erroredexamples: [
    { valid: false, success: true, errors: [array], entity: [object] }
  ],
  success: true,
  failedexamples: []
}

// details on the generated errors
console.log(plantingSpeciesTest.erroredExamples[0].errors)
[{
  instancePath: '/attributes/name',
  schemaPath: '#/properties/attributes/properties/name/enum',
  keyword: 'enum',
  params: { allowedValues: [ 'tomato', 'lettuce', 'corn' ] },
  message: 'must be equal to one of the allowed values'
}]
```

### The overlay for variety.


  We are using the same base type to encode both species and variety. In our farm, we've decided that to make it very clear, when we store a **variety** we always use a binary name, prepended by the **species**. 
  Since we love experimenting with unusual varieties, we can't have an enum as with the species, but at least we can enforce the naming pattern, in which one of the three species is going to be a suffix: "`^((tomato)|(lettuce)|(corn))-`".
  

#### Code

```js
let varietyUUID = randomUUID();
let varietyExample = {
    id: speciesUUID,
    attributes: {
        name: "tomato-black_russian"
    }
};
let varietyError = {
    id: speciesUUID,
    attributes: {
        // let's choose an ill formed pattern
        name: "black_russian-tomato"
    }
};

let plantingVariety = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--plant_type",
    name: "variety",
    validExamples: [varietyExample],
    erroredExamples: [varietyError],
});
// remember to document your work!
plantingVariety.setMainDescription("The variety. The name has a fixed format, prepended by the species in lowercase, separeted by a dash from the variety name.");

// let's limit the species name to the ones satisfying our format.
plantingVariety.setPattern({
    attribute:"name",
    pattern:"^((tomato)|(lettuce)|(corn))-",
    description:"Our farm's format is enforced: the species goes first, than a dash, than the variety.",
});


let plantingVarietyTest = plantingVariety.testExamples();
```

  The tests will reveal again that it seems to work as expected.
  
  
### Fourh and last Overlay: Season

  If you look at the schema for `taxonomy_term--season`, you will see it is very similat to the one we've dealt before.
  In this case, we will enforce a season format using a pattern again.
  Seasons are always a temperate season, separated by a dash from it's beggining year. We will encode it as "((spring)|(summer)|(winter)|(autumn))-20[0-9]{2}".

```js
let seasonUUID = randomUUID();
let seasonExample = {
    id: seasonUUID,
    attributes: {
        name: "spring-2022"
    }
};
let seasonError = {
    id: seasonUUID,
    attributes: {
    // let's use a wrong century
        name: "spring-2122"
    }
};
let season = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--season",
    name: "season",
    validExamples: [seasonExample],
    erroredExamples: [seasonError]
});
season.setMainDescription("A season taxonomy term should exist for each working period, and logs and assets should be associated to the related seasons. Seasons are always a temperate season, separated by a dash from it's beggining year.");

let plantingSeasonTest = season.testExamples();
```

#### This time, we made an error.


  Let's look into the tests.
  
  
```js
console.log(plantingseasontest)
{
  validExamples: [ { valid: true, success: true, entity: [Object] } ],
  erroredExamples: [ { valid: true, success: false, entity: [Object] } ],
  success: false,
  failedExamples: [ { valid: true, success: false, entity: [Object] } ]
}
```


  We can check inside `failedExamples` to see what's wrong.
  
  
```js
console.log(plantingSeasonTest.failedExamples)

[
  {
    // the entitiy was identified as valid
    valid: true,
    // and that's a failure, as it was an errored example
    success: false,
    entity: {
      id: 'e197b910-6ed7-4b10-aba3-337dd84e4177',
      attributes: [Object]
    }
  }
]
```

  After scratching our heads a little, we realize we forgot to enforce the `pattern`. Let's do it.
  
  
  
```js
// let's actually do what we though we had already done
season.setPattern({
    attribute: "name",
    pattern:"((spring)|(summer)|(winter)|(autumn))-20[0-9]{2}",
    description: "Seasons are always a temperate season, separated by a dash from it's beggining year."
});


// test again
plantingSeasonTest = season.testExamples();

// this time we get:


console.log(plantingSeasonTest)

{
  validExamples: [ { valid: true, success: true, entity: [Object] } ],
  erroredExamples: [
    { valid: false, success: true, errors: [Array], entity: [Object] }
  ],
  // success
  success: true,
  failedExamples: []
}
```


## The Big Picture: Covention Schema


  We have defined all the pieces of information we need. 
  Now, we need to assign them a general structure. That's what the `ConventionSchema` Object Class will allow us to do. It is documented [here](https://our-sci.gitlab.io/software/json_schema_distribution/ConventionSchema.html).
  The two main methods we need from it currently are `addAttribute` and `addRelationship`. 
  We will turn each of our relatively simple **overlays** into **attributes** of a bigger schema in the **Covnention**. Once that's done, we will encode the **relationships between each other** using the fields available for each class of entity according to our needs.


### Defining the `ConventionSchema` object.


  This object has a lot of relevant properties. We will briefly explain exach one in comments. Remember we've linked the documentation to the library in case you need more detailed information.
  

```js
let plantingConvention = new builder.ConventionSchema({
    // The title is a natural language name assigned to the convention. It will be used to mention it in the documentation.
    title: "Octavio's Orchard Vegetable Planting",
    // We will keep track of versions in case somebody want's to refer to our work in a stable way.
    version: "0.1.0",
    // this is the name that will be used by all code operations.
    schemaName:"asset--plant--octavio_orchard_planting",
    // This will be used to generate the fundamental '$id' field as a valid link.
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    // The description is a fixed JSON schema property. Here, we are writing it as markdown to make it look more organized in the documentation.
    description:`## Purpose\n
This is the main entity that represents a cultivar. It will be referenced by all subsequent managment logs.`,
    // we wil add the examples later.
    // validExamples: [],
    // erroredExamples: []
});
```


### Adding all of our overlays into fixed roles with the `addAttribute` method

  
  **Attributes** are the special properties of each `entity` for FarmOS. As **conventions** are one level of abstraction up, their attributes are **entities** themselves, which we've described using the overlays.
  Each one will be inserted into our convention object with a name.
  In order to make it more obvious what's what when assigning them, let's first have a look at out **valid convention example**.
  

#### Valid Example for Our Convention


  We could say we've first designed this by hand, in order to make it easy to think around the convention.
  You will see that we've simply filled the attributes directly with our preexisting examples, which are totally valid for each overlay. For `plant_asset` this doesn't work because we need to add something that wasn't contemplated by the **overlay**, **relationships**. Nonetheless, we can still use it with some ingenuity.
  You might be wondering why we asume you know how to write a relationship, but don't worry, you can just look at it superficially now, we will explain the **format for writing relationships** later.
  
```js
let conventionUUID = randomUUID();

let plantingConventionExample = {
    id: conventionUUID,
    // We've already wrote examples for everything!
    season_taxonomy: seasonExample,
    species_taxonomy: speciesExample,
    variety_taxonomy: varietyExample,
    // we need to express our relationships in the plant asset, so we can't directly feed to whole object. Instead:
    plant_asset: {
        id:plantAssetUUID,
        // reuse the attributes
        attributes: validTomato.attributes,
        // write the relationships
        relationships: {
            plant_type: { data: [
                {
                    type: "taxonomy_term--plant_type",
                    id:speciesUUID
                },
                {
                    type: "taxonomy_term--plant_type",
                    id:varietyUUID
                }
            ] },
            season: { data: [
                {
                    type: "taxonomy_term--season",
                    id: seasonUUID
                }
            ] }
        }
    },
};
```

#### Adding the attributes


  We will comment the first one in detail, the procedure is basically the same for each one.
  
  
```js
// add our main entity, the plant_asset.
plantingConvention.addAttribute( {
  // in here, we feed our overlay, represented as an object of the SchemaOverlay class. It is what we've just defined.
  schemaOverlayObject:plantingPlantAsset,
  // this is the role/name we want to assign to our object inside the schema. (check our example below).
  attributeName: "plant_asset",
  // wether this attribute is fundamental for our convention to be valid. Being a planting asset, the plant is understandably enforced.
  required: true 
} );

plantingConvention.addAttribute( { schemaOverlayObject:plantingSpecies, attributeName: "species_taxonomy", required: true } );

// the only remarkable difference here is sometimes we ignore the `variety`, so we've made it non mandatory.
plantingConvention.addAttribute( { schemaOverlayObject:plantingVariety, attributeName: "variety_taxonomy", required: false } );

plantingConvention.addAttribute( { schemaOverlayObject:season, attributeName: "season_taxonomy", required: true } );
```


### Structuring the Convention using relationships


  In this case, all of the `taxonomy_terms` are fed into the `asset--plant` relationship fields, so this is a rather simple structure. 
  
#### Format for relationships in FarmOS

  
  Relationships are fixed fields for each type (you can't create custom ones, as this would make comparability impossible), so in order to get the relationships you want, you will need to choose wisely your main entity.
  
  Relationships are always structured in the same way. The `container entity` has many fields inside it's `relationships` attribute, and inside each one, an array called `data` contains all the relationship descriptors. Said descriptors, like in our example, are always objects with two attributes: a **FarmOS type/bundle pair** and a **UUID** mentioning the specific object we want.
  
  
```js
// let's look again at our example
relationships: {
            plant_type: { data: [
            // notice how this example has 2 entities linked to it under the same field.
                {
                    type: "taxonomy_term--plant_type",
                    id:speciesUUID
                },
                {
                    type: "taxonomy_term--plant_type",
                    id:varietyUUID
                }
            ] },
            season: { data: [
                {
                    type: "taxonomy_term--season",
                    id: seasonUUID
                }
            ] }
        }

```

#### How to add relationships to the Convention object


```js
plantingConvention.addRelationship( {
  // indicates one of the overlays we already added using `addAttribute`. We mention it usint its `attributeName`. It is the container entity because the relationship will be stored inside one of it's `relationship` fields, which should exist in the schema for it.
  containerEntity:"plant_asset",
  // This is the name of a valid (as in, exists in the schema) relationships field from the containerEntity.
  relationName:"plant_type",
  // Call the attribute we want to relate into this one, using also its `attributeName`, assigned when calling `addAttribute` for it.
  mentionedEntity:"species_taxonomy",
  // Wether to accept or reject instantes of our convention missing this particular relationship.
  required: true 
} );

// Since we decided the variety is not mandatory, we won't make this relationship mandatory.
plantingConvention.addRelationship( { containerEntity:"plant_asset" , relationName:"plant_type" , mentionedEntity:"variety_taxonomy" , required: false } );

plantingConvention.addRelationship( { containerEntity:"plant_asset" , relationName:"season" , mentionedEntity:"season_taxonomy" , required: true } );
```

### Adding examples


  Let's write an error example with several ommisions and check what we can do with this `CoventionSchema` object.
  
  
```js
let plantingConventionError = {
    id: conventionUUID,
    plant_asset: {
        id:plantAssetUUID,
        attributes: {
            name: "Example div veg plant asset",
            geometry: null,
            status:"active"
        },
        relationships: {
            plant_type: { data: [
                {
                    type: "taxonomy_term--plant_type",
                    id:varietyUUID
                }
            ] }
        }
    },
    season_taxonomy: seasonError,
    species_taxonomy: speciesError,
    variety_taxonomy: varietyError
};

// add the examples to our convention

plantingConvention.validExamples = [plantingConventionExample];
plantingConvention.erroredExamples = [plantingConventionError];


// evaluating our examples
let conventionTest = plantingConvention.testExamples();

console.log(conventionTest)
// result
{
  validExamples: [ { valid: true, success: true, entity: [Object] } ],
  erroredExamples: [
    { valid: false, success: true, errors: [Array], entity: [Object] }
  ],
  success: true,
  failedExamples: []
}
```


  Since our examples are adequate, we believe these convention is sane. 
  Our **recomendation** is to have several examples of each class, especially if you detect possible errors while working with the convention, or fear any detail might not be correctly enforced. These examples will give you peace of mind later, knowing you've **fully tested your schema**.
  
### A new step: Store your new schemata.

  Once we are satisfied with our new schema, we need to store several files describing it, documentation, etc. 
  The `store` method will do all of that automatically for you, into meaningful folders and with meaningful filenames.
  It will basically call two other methods: first it will test everthing is fine using `testExamples` and produce an error if the examples are not behaving as expected. If the examples are right, it will call a method called `document` to write a complex `mdx` file describing the convention and will add it into the wiki.
  The returned object contains details about the tests (so calling them first really was redundant) and the path under which your convention has finally been stored.
  
```js
let storageOperation = plantingConvention.store();
```


## Deliverables we get to help us share our collection of Conventions

For each **convention** defined this way, we get with 0 intervation:

1. A detailed article in the wiki. In this case, I copied the `mdx` code and [published it here](./asset--plant--octavio_orchard_planting.mdx) so you can compare with what we've parametrized during the tutorial.
2. A validation npm package wich can be used by third parties to check if their data is compliant with our schema without the need to do any extra work.
3. Tests ensuring all of our examples work as expected.
4. The publication of all involved schemata, to be consumed by further users.


## Were to search for more complex examples


  Since everything here is `free software`, we encourage you to simply going to our `definitions` folder and look into all the conventions we've already built. A good wa to search for situations that are interesting to you is by looking into the wiki first for their schemata.
