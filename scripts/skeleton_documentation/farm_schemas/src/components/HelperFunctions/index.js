function dig(object, path, defaultValue = null) {
    const chunks = path
          .split(/\.|\[|\]/)
          .filter(d => d.length !== 0)
    ;
    try {
        const value = chunks.reduce((current, chunk) => current[chunk], object);
        if (value !== undefined) {
            return value;
        } else {
            return defaultValue;
        }
    } catch (e) {
        return defaultValue;
    }
};


/**
 * Given a schema and an attribute, will remove all properties not mentioned in the required array.
 * @param {} schema
 * @param {} attribute
 */
function trimNonRequiredFields(schema, attribute=false) {
    let data;
    if (attribute) {
        data = dig(schema,attribute);
    } else {
        data = schema;
    };
    try { Object.keys( data.properties )
          .filter( d => !["attributes", "required", "relationships"].includes(d) )
          .forEach( key => {
        if (!data.required || !data.required.includes(key)) {
            delete data.properties[key];
        };
    } ); } catch(e) {
        console.log(`couldn't work on ${attribute}`);
        console.log(data);
        new Error(e);
    };
    return schema;
};

/**
 * Given a JSON schema, will create a new JSON schema in which only required fields are kept.
 * @param {} schema
 */
export default function reduceSchemaToRequiredFields(schema) {
    let output = schema;

    // trim at top level, omiting non requied entities
    trimNonRequiredFields(output);

    Object.keys(output.properties).forEach( key => {
        trimNonRequiredFields(output,`properties.${key}`);
        trimNonRequiredFields(output,`properties.${key}.properties.attributes`);
        trimNonRequiredFields(output,`properties.${key}.properties.relationships`);
    } )
    ;
    return output;
};
