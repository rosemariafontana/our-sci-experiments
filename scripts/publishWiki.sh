#!/bin/bash

## $1 is the current branch

export BRANCH=$1

## copy some environment variables into static JSON files (describing the public artifacts)
node scripts/storeRepoData.js

## copy the conventions documentation into the source folder
rm -r scripts/docusaurus/docs/Conventions
cp -r output/documentation/Conventions scripts/docusaurus/docs/Conventions
cp scripts/skeleton_documentation/docs/* scripts/docusaurus/docs/
cp scripts/skeleton_documentation/index.mdx scripts/docusaurus/src/pages/index.mdx
cp scripts/skeleton_documentation/icons scripts/docusaurus/static/img/

## copy the schemata, to feed the schema plugin
rm -r scripts/docusaurus/static/schemas
./scripts/copySchemataToDocusaurus.sh
## processes each schema and creates a new simplified version, only keeping required fields. It is one of the versions we show in documentation
node ./scripts/createRequiredOnlyViews.js
## preparse a json document contaning the structure we want to show in the sidebar for our documentation.
node ./scripts/writeSidebarDocumentationSection.js

cd scripts/docusaurus
npm ci

npm run build

cd ../..

if [ $1 = "main" ]; then
    rm -r public/wiki
    mkdir -p public/wiki
    mv scripts/docusaurus/build/* public/wiki/
elif [ $1 = "staging" ]; then
    rm -r public/staging_wiki
    mkdir -p public/staging_wiki
    mv scripts/docusaurus/build/* public/staging_wiki/
fi
